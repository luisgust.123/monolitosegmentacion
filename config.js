const config = module.exports;

config.express = {
    port: 3002,
    ip: '192.168.34.39'
};

config.mongodb = {
    connect: 'mongodb://luisinei:asd123@cluster0-shard-00-00-d4kvu.mongodb.net:27017,cluster0-shard-00-01-d4kvu.mongodb.net:27017,cluster0-shard-00-02-d4kvu.mongodb.net:27017/segmentacion?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true&w=majority'
};

config.jwt_token={
    secret_key: 'worldisfullofdevelopers',
    expire_time:'3600000',
};